package de.reply.cop.android.data

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlin.random.Random

class DeveloperRepository private constructor() {

    private val _developer = MutableStateFlow(
        listOf(
            Developer("Björn", "Android", "Android Studio"),
            Developer("Digital Nomad Hipster", "JavaScript", "VisualStudio Code"),
            Developer("Füher war alles besser", "C++", "vim"),
        )
    )

    fun streamAllDeveloper(): Flow<List<Developer>> =
        _developer.asStateFlow()
            .randomDelay()


    suspend fun addDeveloper(developer: Developer) {
        delay(600)
        _developer.emit(
            _developer.value.toMutableList()
                .apply { add(developer) }
        )
    }

    fun streamById(id: String): Flow<Developer?> =
        _developer.map { devs -> devs.find { it.id == id } }
            .randomDelay()

    private fun <T> Flow<T>.randomDelay() = onEach { delay(Random.nextLong(100, 600)) }

    companion object {
        val INSTANCE by lazy { DeveloperRepository() }
    }
}