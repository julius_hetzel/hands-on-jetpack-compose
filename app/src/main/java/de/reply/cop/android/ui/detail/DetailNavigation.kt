package de.reply.cop.android.ui.detail

sealed class DetailNavigation {
    object Back : DetailNavigation()
}