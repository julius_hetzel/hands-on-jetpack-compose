package de.reply.cop.android.tracking

import android.util.Log

class Tracker {

    operator fun invoke(event: String) {
        Log.d("tracker", "tracking event: [$event]")
    }
}