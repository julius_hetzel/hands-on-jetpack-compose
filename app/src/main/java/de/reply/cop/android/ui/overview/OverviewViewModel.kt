package de.reply.cop.android.ui.overview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import de.reply.cop.android.data.Developer
import de.reply.cop.android.data.DeveloperRepository
import de.reply.cop.android.tracking.Tracker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class OverviewViewModel(
    private val repository: DeveloperRepository,
    private val trackEvent: Tracker,
) : ViewModel() {

    private val _navigation = MutableSharedFlow<OverviewNavigation>()
    val navigation = _navigation.asSharedFlow()

    private val _developer = MutableStateFlow<List<Developer>>(emptyList())
    val developer = _developer.asStateFlow()

    init {
        repository.streamAllDeveloper()
            .flowOn(Dispatchers.IO)
            .onEach { _developer.emit(it) }
            .launchIn(viewModelScope)
    }

    fun onBackPress() {
        trackEvent("overview -> back")
        viewModelScope.launch {
            _navigation.emit(OverviewNavigation.Back)
        }
    }

    fun onDeveloperClick(developer: Developer) {
        viewModelScope.launch {
            _navigation.emit(OverviewNavigation.DeveloperDetail(developer.id))
        }
    }

    fun onAddDeveloperClick() {
        viewModelScope.launch {
            _navigation.emit(OverviewNavigation.AddDeveloper)
        }
    }
}