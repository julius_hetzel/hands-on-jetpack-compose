package de.reply.cop.android.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.reply.cop.android.data.Developer
import de.reply.cop.android.data.DeveloperRepository
import de.reply.cop.android.tracking.Tracker
import de.reply.cop.android.ui.add.AddDeveloperNavigation
import de.reply.cop.android.ui.overview.OverviewNavigation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class DetailViewModel(
    developerId: String,
    repository: DeveloperRepository,
    private val trackEvent: Tracker,
) : ViewModel() {

    private val _navigation = MutableSharedFlow<DetailNavigation>()
    val navigation = _navigation.asSharedFlow()

    private val _developer = MutableStateFlow<Developer?>(null)
    val developer = _developer.asStateFlow()

    init {
        repository.streamById(developerId)
            .flowOn(Dispatchers.IO)
            .onEach { _developer.emit(it) }
            .launchIn(viewModelScope)
    }

    fun onBackPress() {
        trackEvent("detail -> back")
        viewModelScope.launch {
            _navigation.emit(DetailNavigation.Back)
        }
    }
}