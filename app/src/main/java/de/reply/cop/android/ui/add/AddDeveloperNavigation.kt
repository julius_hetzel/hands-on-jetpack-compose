package de.reply.cop.android.ui.add

sealed class AddDeveloperNavigation {
    object Back : AddDeveloperNavigation()
}