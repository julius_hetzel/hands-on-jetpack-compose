package de.reply.cop.android.ui.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.reply.cop.android.data.Developer
import de.reply.cop.android.data.DeveloperRepository
import de.reply.cop.android.tracking.Tracker
import de.reply.cop.android.ui.overview.OverviewNavigation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddDeveloperViewModel(
    private val repository: DeveloperRepository,
    private val trackEvent: Tracker,
) : ViewModel() {

    private val _navigation = MutableSharedFlow<AddDeveloperNavigation>()
    val navigation = _navigation.asSharedFlow()

    private val _submitEnabled = MutableStateFlow(true)
    val submitEnabled = _submitEnabled.asStateFlow()

    val name = MutableStateFlow("")
    val platform = MutableStateFlow("")
    val ide = MutableStateFlow("")

    fun onSubmitDeveloper() {
        viewModelScope.launch {
            _submitEnabled.emit(false)
            withContext(Dispatchers.IO) {
                repository.addDeveloper(Developer(name.value, platform.value, ide.value))
            }
            _navigation.emit(AddDeveloperNavigation.Back)
            name.emit("")
            platform.emit("")
            ide.emit("")
            _submitEnabled.emit(true)
        }
    }

    fun onBackPress() {
        trackEvent("add-developer -> back")
        viewModelScope.launch {
            _navigation.emit(AddDeveloperNavigation.Back)
        }
    }
}