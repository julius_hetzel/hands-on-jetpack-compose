package de.reply.cop.android.ui.overview

sealed class OverviewNavigation {
    data class DeveloperDetail(val developerId: String) : OverviewNavigation()
    object AddDeveloper : OverviewNavigation()
    object Back : OverviewNavigation()
}