package de.reply.cop.android.data

import java.util.*

data class Developer(
    val name: String,
    val platform: String,
    val ide: String,
    val id: String = UUID.randomUUID().toString()
)